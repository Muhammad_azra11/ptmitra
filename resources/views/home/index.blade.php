<style>
    .table  {
      display: flex;
      /* border: none; */
      padding: 20px;
      padding-top: 5px; 
      justify-content: center;
      
    }

    tr > th{
        text-align: center;
        border: 1px solid black;
        padding: 5px;
    }
    tr > td{
        text-align: center;
        border: 1px solid black;
        padding: 3px;
    }
    tr {
      /* border: 1px solid black; */
        text-align: center;
        /* padding: px; */
    }
    .search {
        border: none;
        border-color: white;
    }
    .search > th > input {
        width: 80px;
        height: 20px;
    }
    .search > th > button {
        width: 40px;
        height: 20px;
        /* border: none; */
        text-align: center;
        align-items: center;
    }
    .search > th {
      border: none;
    }
    .search > th >input {
      /* border: none; */
      width: 100px;
    }
    .transaksi {
      border:  none;
    }

    .transaksi > th {
      border: none;
      /* background-color: rgb(138, 162, 229); */
      padding-top: 345px; 
    }
    .tra {
      border: none;
    }
    .tra > td {
      border: none;
    }
    .tra > td> input {
      background-color: rgb(204, 214, 225);
      border: none;
      /* width: 100px; */
    }
    .transaksii{
      border: none;
    }

    .transaksii > th {
      border-radius: 5px;
      border: none;
      /* border-right: 30px; 
      background-color: rgb(138, 162, 229);  */
      padding-top: 30px;
      padding-bottom: 10px; 
    }

    .btn > a {
      margin: 20px;
      /* padding: 20px; */
      margin-left: 70px;
      top: 10px;
      text-decoration: none;
      color: black;
      /* border: none; */
      padding: 8px;
      background-color: rgb(246, 218, 218);
      border-radius: 5px;
      /* box-shadow: 0px,0px,13px,4px; */
    }

    /* .btn > button:hover{
      cursor: pointer;
    } */

    .judul {
      font-size: medium;
      font-family: 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif;
      text-align: center;
    }
    .success {
      margin: 7px;
      font-family: Verdana, Geneva, Tahoma, sans-serif;
      background-color: rgb(87, 230, 87);
      text-align: center;
      padding: 4px;
      color: rgb(255, 255, 255);
    }

    @media (max-width: 767px){
      .table {
        display: block;
      }
      .judul{
        display: flex;
        justify-content: center;
        text-align: center;
        margin-left: 200px;
      }
    }
</style>

@if(Session::has('status'))
<div class="success">
  {{Session::get('massage')}}
</div>
  @endif

<title>PT. Mitra Sinerji Teknoindo</title>
<div class="btn">
  <a href="/transaksi">+ Transaksi</a>
</div>

<h1 class="judul">DAPARTEMET PRODUKSI</h1>
{{-- <div class="table-wrapper"> --}}
<table class="table" style="border-collapse: collapse;">
    <tr class="search">
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <form action="/transaksi/cari" method="GET">
        <th><button type="submit">Cari</button></th>
        <th><input type="text" placeholder="Nama Customer" name="cari"></th>
      </form>
    </tr>

    <tr style="background-color:rgb(246, 218, 218)">
      <th>No</th>
      <th>No Transaksi</th>
      <th>Tanggal</th>
      <th>Nama Customer</th>
      <th>No Telp</th>
      <th>Jumlah Barang</th>
      <th>Sub Total</th>
      <th>Diskon</th>
      <th>Ongkir</th>
      <th>Total</th>
    </tr>
    @foreach ($hasil as $item)
    <tr>
      <td>{{ $loop->iteration }}</td>
      <td>{{$item->no_transaksi}}</td>
      <td>{{$item->tanggal}}</td>
      <td>{{$item->nama_customer}}</td>
      <td>{{$item->telp}}</td>
      <td>{{$item->jumlah_barang}}</td>
      <td>{{$item->sub_total}}</td>
      <td>{{$item->diskon}}</td>
      <td>{{$item->ongkir}}</td>
      <td>{{$item->total_bayar}}</td>
    </tr>
    @endforeach

    <tr style="background-color: rgb(219, 215, 215)">
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td colspan="2">Grand Total</td>
      <td colspan="2">@currency($hasil->pluck('total_bayar')->sum())</td>
    </tr>

    <tr class="transaksi">
      <th>Transaksi</th>
    </tr>
    <tr class="tra">
      <td>No</td>
      <form action="/transaksi/no" method="GET">
        <td><input type="text" name="no"></td>
      </form>
    </tr>
    <tr class="tra">
      <td>Tanggal</td>
      <td><input type="text" name="" id=""></td>
    </tr>

    <tr class="transaksii">
      <th>Customer</th>
    </tr>
    <tr class="tra">
      <td>Kode</td>
      <form action="/transaksi/kode" method="GET">
        <td><input type="text" name="kode"></td>      
      </form>
    </tr>
    <tr class="tra">
      <td>Nama</td>
      <td><input type="text" name="" id=""></td>
    </tr>
    <tr class="tra">
      <td>Telp</td>
      <td><input type="text"></td>
    </tr>
  </table>





  <style>
    table  {
      display: flex;
      padding: 20px;
      box-shadow: 12px, 9px, 4px;
      margin-top: 50px; 
      /* padding-top: 85px;  */
      justify-content: center;
      
    }

    tr > th{
        text-align: center;
        border: 1px solid black;
        padding: 5px;
    }
    tr > td{
        text-align: center;
        border: 1px solid black;
        padding: 3px;
    }
    tr {
        text-align: center;
        border: 1px solid black;
        /* padding: px; */
    }
    tr > .option > a {
        padding: 3px;
        text-decoration: none;
    }
    
    tr >.bawah {
        /* display: flex; */
        padding-top: 10px;
        /* margin-top: 100px; */
        border: none;
    }
     tr > td >input {
      width: 70px;
    }
    tr > .none {
      border: none;
    }
    tr > .none .bold {
      text-align: center;
      font-weight: 700;
    }
    .tr {
      border: none;
    }
    @media (max-width: 767px){
      table {
        display: block;
      }
    }

</style>

<form id="form1" name="form1" action="">
<table style="border-collapse: collapse;">
    <tr>
       <th class="warna" colspan="2"  style="background-color: rgb(219, 215, 215)">Tambah</th>
      <th>No</th>
      <th>Kode Barang</th>
      <th>Nama Barang</th>
      <th>Qty</th>
      <th>Harga Bandrol</th>
      <th>Diskon (%)</th>
      <th>Diskon (Rp)</th>
      <th>Harga Diskon</th>
      <th>Total</th>
    </tr>
    <tr>
      @foreach ($hasil as $item)
      <td class="option" style="background-color: rgb(219, 215, 215)"><a href="/transaksi/edit/{{$item->id}}">Ubah</a></td>
      <td  class="option" style="background-color: rgb(219, 215, 215)"><a href="/transaksi/hapus/{{$item->id}}"  onclick="return confirm('Apakah Anda Yakin Menghapus Data?');">Hapus</a></td>
      <td>{{$item->sales_id}}</td>
      <td>{{$item->kode_barang}}</td>
      <td>{{$item->nama_barang}}</td>
      <td>{{$item->qty}}</td>
      <td>{{$item->harga_bandrol}}</td>
      <td>{{$item->diskon_pct}}</td>
      <td>{{$item->diskon_nilai}}</td>
      <td>{{$item->harga_diskon}}</td>
      <td>@currency($item->total_harga)</td>    
    </tr>
    @endforeach

    <tr class="tr" >
        <td class="bawah"></td>
        <td class="bawah"></td>
        <td class="bawah"></td>
        <td class="bawah"></td>
        <td class="bawah"></td>
        <td class="bawah"></td>
        <td class="bawah"></td>
        <td class="bawah"></td>
        <td class="bawah">Sub Total</td>
        <td class="bawah"></td>
        <td class="none"><input type="number" name="total_harga" value="{{$hasil->pluck('total_harga')->sum()}}"  onfocus="startCalculate()" onblur="stopCalc()" readonly></td>
    </tr>
    <tr class="tr">
        <td class="bawah"></td>
        <td class="bawah"></td>
        <td class="bawah"></td>
        <td class="bawah"></td>
        <td class="bawah"></td>
        <td class="bawah"></td>
        <td class="bawah"></td>
        <td class="bawah"></td>
        <td class="bawah">Diskon</td>
        <td class="bawah"></td>
        <td class="none"><input type="number" placeholder="Rp" name="diskon" id="diskon" value="0" onfocus="startCalculate()" onblur="stopCalc()"></td>
    </tr>
    <tr class="tr">
        <td class="bawah"></td>
        <td class="bawah"></td>
        <td class="bawah"></td>
        <td class="bawah"></td>
        <td class="bawah"></td>
        <td class="bawah"></td>
        <td class="bawah"></td>
        <td class="bawah"></td>
        <td class="bawah">Ongkir</td>
        <td class="bawah"></td>
        <td class="none"><input type="number" placeholder="Rp" name="ongkir" id="ongkir" value="0" onfocus="startCalculate()" onblur="stopCalc()" ></td>
        {{-- <td class="bawah">d</td>
        <td class="bawah">d</td> --}}
    </tr>
    <tr class="tr">
        <td class="bawah"></td>
        <td class="bawah"></td>
        <td class="bawah"></td>
        <td class="bawah"></td>
        <td class="bawah"></td>
        <td class="bawah"></td>
        <td class="bawah"></td>
        <td class="bawah"></td>
        <td class="bawah">Total Bayar</td>
        <td class="bawah"></td>
        {{-- <td class="none" name="total_bayar">@currency($hasil->pluck('total_harga')->sum())</td> --}}
        <td class="none"><input class="bold" type="number" name="total_bayar" value="{{$hasil->pluck('total_harga')->sum()}}" readonly  onfocus="startCalculate()" onblur="stopCalc()"></td>
        {{-- <td class="bawah">d</td>
        <td class="bawah">d</td> --}}
    </tr>
  </table>
</form>

  <script type="text/javascript">
     function startCalculate(){
      interval=setInterval("Calculate()",10);
      }
      
      function Calculate(){
          var diskon = document.form1.diskon.value;
          var ongkir = document.form1.ongkir.value;
          var ambil2 = document.form1.total_harga.value;
          var all = ambil2 - parseInt(diskon) + parseInt(ongkir); 
          document.form1.total_bayar.value=(all);
          // document.form1.total_bayar.value=(ambil2);
      } 
      
      function stopCalc(){
      clearInterval(interval);
      }
  </script>