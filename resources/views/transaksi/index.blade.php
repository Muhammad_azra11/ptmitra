<!DOCTYPE html>
<html>
  <head>
    <title>Form Transaksi</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    <style>
      html, body {
      display: flex;
      justify-content: center;
      font-family: Roboto, Arial, sans-serif;
      font-size: 15px;
      }
      form {
      border: 5px solid #f1f1f1;
      }
      input[type=text], input[type=number], input[type=datetime-local] {
      width: 100%;
      padding: 16px 8px;
      margin: 8px 0;
      display: inline-block;
      border: 1px solid #ccc;
      box-sizing: border-box;
      }
      button {
      background-color: #1480cd;
      color: white;
      padding: 14px 0;
      margin: 10px 0;
      border: none;
      cursor: grabbing;
      width: 100%;
      }
      h1 {
      text-align:center;
      fone-size:18;
      }
      button:hover {
      opacity: 0.8;
      }
      .formcontainer {
      text-align: left;
      margin: 24px 50px 12px;
      }
      .container {
      padding: 16px 0;
      text-align:left;
      }
      span.psw {
      float: right;
      padding-top: 0;
      padding-right: 15px;
      }
      /* Change styles for span on extra small screens */
      @media screen and (max-width: 300px) {
      span.psw {
      display: block;
      float: none;
      }
    }
    .h2 {
        text-align: center;
        font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif; 
    }
    .success {
      margin: 7px;
      font-family: Verdana, Geneva, Tahoma, sans-serif;
      background-color: rgb(87, 230, 87);
      text-align: center;
      padding: 4px;
      color: rgb(255, 255, 255);
      /* width: 250px;
      height: 20px; */
    }
    .kembali {
      background-color: #1480cd;
      color: white;
      /* padding: 14px 0; */
      /* margin: 10px 0; */
      /* border: none; */
      cursor: pointer;
      width: 80px;
      height: 25px;
      text-align: center;
      padding: 3px;
      border-radius: 5px;
    }
    .kembali > a {
      text-decoration: none;
      color: rgb(255, 255, 255);
    }
    </style>
  </head>
  <body>
    <form id="form1" name="form1" action="transaksi" method="POST">
        @csrf

        @if(Session::has('status'))
      <div class="success">
        {{Session::get('massage')}}
      </div>
        @endif

        <h1>Transaksi Pt.Mitra</h1>
        <div class="formcontainer">
            <hr/>
            <div class="container">
        <label for="no_transaksi"><strong>No Transaksi</strong></label>
        <input type="text" placeholder="No Transaksi" name="no_transaksi" required>
        <label for="tanggal"><strong>Tanggal</strong></label>
        <input type="datetime-local" placeholder="MM/DD/YYYY" value="{{now()}}" name="tanggal" required>
        <label for="nama_customer"><strong>Nama Customer</strong></label>
        <input type="text" placeholder="Nama Customer" name="nama_customer" required>
        <label for="telp"><strong>No Telpon</strong></label>
        <input type="number" placeholder="No Telpon" name="telp" required>
        <label for="jumlah_barang"><strong>Jumlah Barang</strong></label>
        <input type="number" placeholder="Jumlah Barang" name="jumlah_barang" id="jumlah_barang" onfocus="startCalculate()" onblur="stopCalc()" required>
        <label for="sub_total"><strong>Sub Total</strong></label>
        <input type="number" placeholder="Sub Total" readonly name="sub_total" id="sub_total" onfocus="startCalculate()" onblur="stopCalc()" required>
        <label for="diskon"><strong>Diskon</strong></label>
        <input type="number" placeholder="Diskon" name="diskon" onfocus="startCalculate()" onblur="stopCalc()" required>
        <label for="ongkir"><strong>Ongkir</strong></label>
        <input type="number" placeholder="Ongkir" name="ongkir" value="0" onfocus="startCalculate()" onblur="stopCalc()" required>
        <label for="total_bayar"><strong>Total Bayar</strong></label>
        <input type="number" placeholder="Total Bayar" readonly name="total_bayar" onfocus="startCalculate()" onblur="stopCalc()" required>
      </div>

      <h2 class="h2">Form Input</h2>
      <label for="total_bayar"><strong>Sales Id</strong></label>
      <input type="number" placeholder="Sales Id" value="1" name="sales_id" required>
      <label for="kode_barang"><strong>Kode Barang</strong></label>
      <input type="text" placeholder="Kode Barang" name="kode_barang" required>
      <label for="nama_barang"><strong>Nama Barang</strong></label>
      <input type="text" placeholder="Nama Barang" name="nama_barang" required>
      <label for="qty"><strong>Qty</strong></label>
      <input type="number" placeholder="Qty" name="qty" value="2"onfocus="startCalculate()" onblur="stopCalc()" readonly required>
      <label for="harga_bandrol"><strong>Harga Bandrol</strong></label>
      <input type="number" placeholder="Harga Bandrol" name="harga_bandrol" onfocus="startCalculate()" onblur="stopCalc()" required>
      <label for="diskon_pct"><strong>Diskon (%)</strong></label>
      <input type="number" placeholder="Diskon (%)" name="diskon_pct"  onfocus="startCalculate()" onblur="stopCalc()" required>
      <label for="diskon_nilai"><strong>Diskon (Rp)</strong></label>
      <input type="number" placeholder="Diskon (Rp)" name="diskon_nilai" onfocus="startCalculate()" onblur="stopCalc()" required>
      <label for="harga_diskon"><strong>Harga Diskon</strong></label>
      <input type="number" placeholder="Harga Diskon" name="harga_diskon" onfocus="startCalculate()" onblur="stopCalc()">
      <label for="total_harga"><strong>Total Harga</strong></label>
      <input type="number" placeholder="Harga Harga" onfocus="startCalculate()" onblur="stopCalc()" name="total_harga" required>
      <button type="submit">Input</button>
      <div class="kembali">
        <a href="/">Kembali</a>
      </div>
      {{-- <div class="container" style="background-color: #eee">
        <label style="padding-left: 15px">
        <input type="checkbox"  checked="checked" name="remember"> Remember me
        </label>
        <span class="psw"><a href="#"> Forgot password?</a></span>
      </div> --}}
    </form>

    <script type="text/javascript">
      function startCalculate(){
      interval=setInterval("Calculate()",10);
      }
      
      function Calculate(){
        var jumlah_barang=document.form1.jumlah_barang.value;
        document.form1.sub_total.value=(100*jumlah_barang);
        var sub_total = document.form1.sub_total.value;
        var diskon = document.form1.diskon.value;
        var ongkir = document.form1.ongkir.value;
        var o = parseInt(sub_total) + parseInt(ongkir); 
          var satu = (sub_total)-(sub_total*(diskon/100));
          var dua = parseInt(satu) + parseInt(ongkir) ;
          document.form1.total_bayar.value=(dua);
        var qty = document.form1.jumlah_barang.value;
        document.form1.qty.value=(qty);
          //document.form1.total_bayar.value=(sub_total)-(sub_total*(diskon/100));
        // document.form1.total_bayar.value=(sub_total*(diskon/100));

      // var a=document.form1.harga_perunit.value;

      var harga_bandrol = document.form1.harga_bandrol.value;
      var diskon_nilai = document.form1.diskon_nilai.value;
      var tambah = harga_bandrol * qty - diskon_nilai;
      var diskon_pct = document.form1.diskon_pct.value;
      var hasil1 =  (tambah)-(tambah*(diskon_pct/100));
      var hasil =  (tambah)-(tambah*(diskon_pct/100));
      // tambah = parseInt(tambah);
      // diskon_pct = parseInt(diskon_pct);
      // var hasil = tambah * (diskon_pct/100); 
      // hasil = harga - parseInt(hasil);
      document.form1.harga_diskon.value=hasil1;
      document.form1.total_harga.value=(hasil);
      } 
      
      function stopCalc(){
      clearInterval(interval);
      }
      </script>

  </body>
</html>