<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class transaksi extends Model
{
    use HasFactory;

    protected $table = 'transaksis';
    protected $fillable = [
      'no_transaksi',
      'tanggal',
      'nama_customer',
      'telp',
      'jumlah_barang',
      'sub_total',
      'diskon',
      'ongkir',
      'total_bayar',
      'sales_id',
      'kode_barang',
      'nama_barang',
      'qty',
      'harga_bandrol',
      'diskon_pct',
      'diskon_nilai',
      'harga_diskon',
      'total_harga',
    ];
}
