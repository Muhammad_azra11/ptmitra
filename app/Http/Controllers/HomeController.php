<?php

namespace App\Http\Controllers;

use App\Models\transaksi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index(Request $request) {
        
        $cari = $request->cari;
        $hasil = DB::table('transaksis')
	    ->where('nama_customer','like',"%".$cari."%")
	    ->paginate();
        
        return view('home.index', compact('hasil'));
    }

    public function no(Request $request) {
        
        $no = $request->no;
        $hasil = DB::table('transaksis')
	    ->where('sales_id','like',"%".$no."%")
	    ->paginate();  

        return view('home.index', compact('hasil'));
    }
    public function kode(Request $request) {
        
        $kode = $request->kode;
        $hasil = DB::table('transaksis')
	    ->where('kode_barang','like',"%".$kode."%")
	    ->paginate();  

        return view('home.index', compact('hasil'));
    }

    public function edit($id) 
    {
      $hasil = transaksi::findOrFail($id);
      return view('home.edit', compact('hasil'));
    }

    public function update(Request $request, $id) {
        $hasil = transaksi::findOrFail($id);
        $hasil->update($request->all());

        if($hasil){
            Session()->flash('status', 'success');
            Session()->flash('massage', 'Update Successfuly!!');
        }
        return redirect()->back();
    }

    public function destroy($id) 
    {
        $hasil = transaksi::where('id', $id)
        ->delete();

        if($hasil){
            Session()->flash('status', 'success');
            Session()->flash('massage', 'hapus Successfuly!!');
        }

        return redirect()->back();
    }

}
