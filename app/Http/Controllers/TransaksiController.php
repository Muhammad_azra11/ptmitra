<?php

namespace App\Http\Controllers;

use App\Models\transaksi;
use Illuminate\Http\Request;

class TransaksiController extends Controller
{
    public function index()
    {
        return view('transaksi.index');
    }
    
    public function store(Request $request)
    {
        // dd($request->all());
      $transaksi = transaksi::create($request->all());

      if($transaksi){
        Session()->flash('status', 'success');
        Session()->flash('massage', 'Berhasil Transaksi!!');
    }
      return redirect('transaksi');
    }
}
