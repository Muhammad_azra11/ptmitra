<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\TransaksiController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [HomeController::class, 'index']);
Route::get('/transaksi', [TransaksiController::class, 'index']);
Route::post('/transaksi', [TransaksiController::class, 'store']);
Route::get('/transaksi/edit/{id}', [HomeController::class, 'edit']);
Route::put('/transaksi{id}', [HomeController::class, 'update'])->name('transaksi.update');
Route::get('/transaksi/hapus/{id}', [HomeController::class, 'destroy']);
Route::get('/transaksi/cari', [HomeController::class, 'index']);
Route::get('/transaksi/no', [HomeController::class, 'no']);
Route::get('/transaksi/kode', [HomeController::class, 'kode']);

